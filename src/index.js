require('dotenv').config();

const express = require('express');
const multer = require('multer');
const moment = require('moment-timezone');
const session = require('express-session');
const upload = multer({dest: __dirname + '/../tmp_uploads/'});
const upload_img = require(__dirname + '/modules/upload-img-module');

const app = express();

app.set('view engine', 'ejs');

app.use(session({
    saveUninitialized: false,
    resave: false,
    secret: 'askdfjks39485394dkjglkdkfkjkkk',
    cookie: {
        maxAge: 1200000
    }
}));
app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(require('cors')());


app.use((req, res, next)=>{
    res.locals.title = '小新的購物網';
    res.locals.pageName = '';
    next();
});

app.get('/', (req, res)=>{
    res.locals.pageName = 'home';
    // res.send('Hello Express');
    res.render('home', {name: 'Shinder>'});
});

app.get('/pending', ()=>{});

app.get('/json-sales', (req, res)=>{
    res.locals.pageName = 'json-sales';
    res.locals.title = 'JSON SALES - ';
    const data = require(__dirname + '/../data/sales');
    res.render('json-sales', {sales: data})
});

app.get('/try-qs', (req, res)=>{
    res.json(req.query);
});


app.post('/try-post',(req, res)=>{
    res.json(req.body);
});

app.get('/try-post-form',(req, res)=>{
    res.render('try-post-form');
});

app.post('/try-post-form',(req, res)=>{
    res.render('try-post-form', req.body);
});

app.post('/try-upload', upload_img.single('avatar'), (req, res)=>{
    // res.json(req.file);

    if(req.file && req.file.filename){
        res.json({
            body: req.body,
            filename: req.file.filename,
        });
    } else {
        res.json({
            error: '檔案不是圖片檔',
        });
    }
});

app.post('/try-uploads', upload_img.array('photos'), (req, res)=>{
    // res.json(req.files);

    const files = [];
    if(req.files && req.files.length){
        req.files.forEach(f=>{
            files.push(f.filename);
        })
    }
    res.json({
        body: req.body,
        files: files,
    });

});

app.get('/my-params1/:action?/:id?', (req, res)=>{
    res.json(req.params);
});

app.get(/^\/m\/09\d{2}-?\d{3}-?\d{3}$/i, (req, res)=>{
    let u = req.url.split('?')[0];
    u = u.slice(3).split('-').join('');
    res.send(u);
});

app.use('/abcd', require(__dirname + '/routes/admin2'));
app.use('/address-book', require(__dirname + '/routes/address_book'));

app.get('/try-session', (req, res)=>{
    req.session.my_var = req.session.my_var || 0;
    req.session.my_var++;
    res.json({
        my_var: req.session.my_var,
        session: req.session
    });

});
app.get('/try-moment', (req, res)=>{
    const fm = 'YYYY-MM-DD HH:mm:ss';
    const now = moment(new Date());

    res.json({
        now1: now.format(fm),
        now2: now.tz('Europe/London').format(fm),
    });
});


app.use(express.static(__dirname + '/../public'));
// 404
app.use((req, res)=>{
    // res.type('text/html');
    res.status(404).send('<h1>找不到頁面</h1>');
});

app.listen(process.env.EXPRESS_PORT, ()=>{
    console.log('server started ~', process.env.EXPRESS_PORT);
})