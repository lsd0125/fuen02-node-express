const express = require('express');
const moment = require('moment-timezone');
const db = require(__dirname + '/../modules/tedious_connect');
const upload_img = require(__dirname + '/../modules/upload-img-module');
const {TYPES} = require("tedious");
const email_pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

const router = express.Router();

async function getListData(){
    const sql = "SELECT * FROM address_book ORDER BY sid DESC";

    const rs = await db.myExecSql(sql);

    rs.rows.forEach(el=>{
        el.birthday2 = moment(el.birthday).format('YYYY-MM-DD');
    });
    return rs;
}

router.get('/', async (req, res)=>{
    res.locals.pageName = 'ab/list';
    //const t_sql = "SELECT COUNT(1) num FROM address_book";

    const rs = await getListData();
    res.render('address-book/list', {rows: rs.rows});
});

router.get('/api/list', async (req, res)=>{
    const rs = await getListData();
    res.json(rs);
});

router.get('/edit/:sid', async (req, res)=>{
    const sql = "SELECT * FROM address_book WHERE sid=@sid";
    const result = await db.myExecSql(sql, [
        [TYPES.Int, req.params.sid]
    ]);

    if(result.rows.length){
        result.rows[0].birthday = moment(result.rows[0].birthday).format('YYYY-MM-DD');
        res.render('address-book/edit', {row: result.rows[0]});
    } else {
        res.redirect('/address-book');
    }
});

router.post('/edit/:sid', upload_img.none(), async (req, res)=>{
    const output = {
        success: false,
        body: req.body
    };
    // TODO: 檢查欄位的格式
    if(! email_pattern.test(req.body.email)){
        output.error = 'Email 格式不符';
        return res.json(output);
    }

    const sql = `UPDATE address_book SET name=@name, email=@email, mobile=@mobile, birthday=@birthday, address=@address WHERE sid=@sid`;
    const result = await db.myExecSql(sql, [
        [TYPES.NVarChar, req.body.name],
        [TYPES.NVarChar, req.body.email],
        [TYPES.NVarChar, req.body.mobile],
        [TYPES.Date, req.body.birthday],
        [TYPES.NVarChar, req.body.address],
        [TYPES.Int, req.params.sid],
    ]);
    if(result.rowCount===1){
        output.success = true;
    }
    output.result = result;
    res.json(output);
});

router.get('/add', (req, res)=> {
    res.locals.pageName = 'ab/add';
    res.render('address-book/add');
});

router.post('/add', upload_img.none(), async (req, res)=> {
    const output = {
        success: false,
        code: 0,
        error: ''
    };

    // 檢查資料格式
    if(! email_pattern.test(req.body.email)){
        output.code = 410;
        output.error = '電郵格式錯誤';
        return res.json(output);
    }

    if(! moment(req.body.birthday, 'YYYY-MM-DD', true).isValid()){
        output.code = 420;
        output.error = '生日格式錯誤';
        return res.json(output);
    }

    const sql = `INSERT INTO address_book (
            name, email, mobile, birthday, address, created_at
        ) VALUES (
            @name, @email, @mobile, @birthday, @address, GETDATE()
        )`;

    const result = await db.myExecSql(sql, [
        [TYPES.NVarChar, req.body.name],
        [TYPES.NVarChar, req.body.email],
        [TYPES.NVarChar, req.body.mobile],
        [TYPES.Date, req.body.birthday],
        [TYPES.NVarChar, req.body.address],
    ]);
    if(result.rowCount===1){
        output.success = true;
    }
    res.json(output);
});

router.get('/del/:sid', async (req, res)=>{
    const sql = "DELETE FROM address_book WHERE sid=@sid";
    const result = await db.myExecSql(sql, [
        [TYPES.Int, req.params.sid]
    ]);
    if(req.get('Referer')){
        res.redirect( req.get('Referer') );
    } else {
        res.redirect('/address-book/');
    }
})
module.exports = router;