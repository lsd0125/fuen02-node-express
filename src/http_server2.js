const http = require('http');
const fs = require('fs');

const server = http.createServer((req, res)=>{
    fs.writeFile(
        __dirname + '/headers.txt',
        JSON.stringify(req.headers),
        (error)=>{
            res.end('<h2>ok</h2>');
        });
});

server.listen(3000);